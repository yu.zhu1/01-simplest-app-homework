package com.twuc.webApp;

public class CalculateTable {
    StringBuilder calculateTable(String calcMode) {
        StringBuilder multiTable = new StringBuilder();
        int i, j, sum;

        for (i = 1; i <= 9; i++) {
            for (j = 1; j <= i; j++) {
                sum = i * j;
                multiTable.append(i).append("*").append(j).append("=").append(sum);

                int sumLength = String.valueOf(sum).length();
                if (sumLength == 2) {
                    multiTable.append(" ");
                } else {
                    multiTable.append("  ");
                }
            }
            multiTable.append("<br>");
        }
    }
}
