package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;


@RestController
public class MultiplicationTable {
    @GetMapping("/api/tables/multiply")
    String getMultiplicationTable() {
//        httpServletResponse.setContentType("text/plain");
        StringBuilder multiTable = new StringBuilder();
        int i, j, sum;

        for (i = 1; i <= 9; i++) {
            for (j = 1; j <= i; j++) {
                sum = i * j;
                multiTable.append(i).append("*").append(j).append("=").append(sum);

                int sumLength = String.valueOf(sum).length();
                if (sumLength == 2) {
                    multiTable.append(" ");
                } else {
                    multiTable.append("  ");
                }
            }
            multiTable.append("<br>");
        }
        return multiTable.toString();
    }


}
