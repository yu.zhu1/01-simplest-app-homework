package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;


@RestController
public class AddictionTable {
    @GetMapping("/api/tables/plus")
    String getAddictionTable(HttpServletResponse httpServletResponse) {
        httpServletResponse.setContentType("text/plain");
        StringBuilder addTable = new StringBuilder();
        int i, j, sum;

        for (i = 1; i <= 9; i++) {
            for (j = 1; j <= i; j++) {
                sum = i + j;
                addTable.append(i).append("+").append(j).append("=").append(sum);

                int sumLength = String.valueOf(sum).length();
                if (sumLength == 2) {
                    addTable.append(" ");
                } else {
                    addTable.append("  ");
                }
            }
            addTable.append("<br>");
        }
        return addTable.toString();
    }
}