package com.twuc.webApp;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddictionTableTest {
    @Test
    void should_return_addiction_table_when_get() {
        AddictionTable addictionTable = new AddictionTable();
        String addictResult = addictionTable.getAddictionTable();
        assertEquals("1+1=2  <br>" +
                "2+1=3  2+2=4  <br>" +
                "3+1=4  3+2=5  3+3=6  <br>" +
                "4+1=5  4+2=6  4+3=7  4+4=8  <br>" +
                "5+1=6  5+2=7  5+3=8  5+4=9  5+5=10 <br>" +
                "6+1=7  6+2=8  6+3=9  6+4=10 6+5=11 6+6=12 <br>" +
                "7+1=8  7+2=9  7+3=10 7+4=11 7+5=12 7+6=13 7+7=14 <br>" +
                "8+1=9  8+2=10 8+3=11 8+4=12 8+5=13 8+6=14 8+7=15 8+8=16 <br>" +
                "9+1=10 9+2=11 9+3=12 9+4=13 9+5=14 9+6=15 9+7=16 9+8=17 9+9=18 <br>", addictResult);
    }
}